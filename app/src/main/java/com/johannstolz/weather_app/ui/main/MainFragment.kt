package com.johannstolz.weather_app.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.button.MaterialButtonToggleGroup
import com.johannstolz.weather_app.Constants.Companion.BAD_RESPONSE
import com.johannstolz.weather_app.Constants.Companion.NOT_SET
import com.johannstolz.weather_app.Constants.Companion.NOT_SET_NO_LOCATION
import com.johannstolz.weather_app.MainActivity
import com.johannstolz.weather_app.R
import com.johannstolz.weather_app.data.model.UiWeatherData
import com.johannstolz.weather_app.data.model.WeatherData
import com.johannstolz.weather_app.ui.SharedViewModel
import com.johannstolz.weather_app.ui.SharedViewModelFactory
import com.johannstolz.weather_app.utils.*
import com.squareup.picasso.Picasso

class MainFragment : Fragment() {
    private val sharedViewModel: SharedViewModel by activityViewModels {
        SharedViewModelFactory(InjectorUtils.getCityDataRepository(requireContext()))
    }
    private lateinit var mainContainer: ConstraintLayout
    private lateinit var mainViewModel: MainViewModel

    private val onLayoutChangeListener: View.OnLayoutChangeListener
        get() = View.OnLayoutChangeListener { _: View, _: Int, _: Int, _: Int, _: Int, _: Int, _: Int, _: Int, _: Int -> setSquare() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel = ViewModelProvider(
            this,
            MainViewModelFactory.getInstance(InjectorUtils.getWeatherRepository())
        ).get(MainViewModel::class.java)

        if (!mainViewModel.isHoldingSearchData) {
            (activity as MainActivity).getLastLocation()
        }

    }

    override fun onResume() {
        super.onResume()
        mainContainer.addOnLayoutChangeListener(onLayoutChangeListener)
    }

    override fun onPause() {
        super.onPause()
        mainContainer.removeOnLayoutChangeListener(onLayoutChangeListener)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)

        mainContainer = root.findViewById(R.id.main_weather_data_layout)

        val switcher: MaterialButtonToggleGroup = root.findViewById(R.id.switch_material)
        val degree = getString(R.string.degree)
        val mainDataTemp: TextView = root.findViewById(R.id.main_weather_data_temp)
        switcher.addOnButtonCheckedListener { group, checkedId, isChecked ->
            if (isChecked) {
                when (checkedId) {
                    R.id.btn_celsius -> (mainViewModel.weatherData.value?.toUiWeatherData?.temperature?.kelvinToC.toString() + degree).also {
                        mainDataTemp.text = it
                    }
                    R.id.btn_fahrenheit -> (mainViewModel.weatherData.value?.toUiWeatherData?.temperature?.kelvinToF.toString() + degree).also {
                        mainDataTemp.text = it
                    }
                }
            }
        }

        val editText: TextView = root.findViewById(R.id.edit_text)
        editText.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                IME_ACTION_DONE -> {
                    mainViewModel.fetchData(editText.text.toString())
                    editText.text = ""
                    editText.isCursorVisible = false
                }
            }
            false
        }

        editText.setOnClickListener {
            if (!editText.isCursorVisible) {
                editText.isCursorVisible = true
            }
        }

        val myLocation: TextView = root.findViewById(R.id.my_location)
        myLocation.setOnClickListener {
            mainViewModel.isHoldingSearchData = false
            (activity as MainActivity).getLastLocation()
        }

        observeVieModel(root)

        return root
    }

    private fun observeVieModel(root: View) {
        sharedViewModel.homeLocation.observe(viewLifecycleOwner, {
            when (it) {
                NOT_SET -> {
                    if (mainViewModel.weatherData.value != null) {
                        displayDialog(
                            R.string.title_home_city_not_set,
                            R.string.does_this_city_home, requireContext(),
                            runnableAcceptHomeLocation(
                                mainViewModel.weatherData.value
                            ),
                            runnableDeclineHomeLocation()
                        )
                    }
                }
                NOT_SET_NO_LOCATION -> {
                    displayDialogOneBtn(
                        R.string.no_location_detected,
                        R.string.no_home_city_set, requireContext(),
                        runnableDeclineHomeLocation()
                    )

                }
            }
        })

        sharedViewModel.currentLocation.observe(viewLifecycleOwner, {
            if (!mainViewModel.isHoldingSearchData) {
                mainViewModel.fetchData(it)
            }
        })

        mainViewModel.errorMessage.observe(viewLifecycleOwner, {
            if (it.contains("404") || it.contains("400")) {
                makeShortToast(requireContext(), BAD_RESPONSE)
            }
        })
        mainViewModel.weatherData.observe(viewLifecycleOwner, {
            handleViews(it.toUiWeatherData, root)
        })

    }

    private fun handleViews(it: UiWeatherData, root: View) {

        val city: TextView = root.findViewById(R.id.city_name)
        city.text = it.cityName

        val mainDataTemp: TextView = root.findViewById(R.id.main_weather_data_temp)
        val switcher: MaterialButtonToggleGroup = root.findViewById(R.id.switch_material)
        val degree = getString(R.string.degree)
        when (switcher.checkedButtonId) {
            R.id.btn_celsius -> (it.temperature.kelvinToC.toString() + degree).also {
                mainDataTemp.text = it
            }
            R.id.btn_fahrenheit -> (it.temperature.kelvinToF.toString() + degree).also {
                mainDataTemp.text = it
            }
        }


        val humidityData: TextView = root.findViewById(R.id.humidity_data)
        humidityData.text = it.humidity

        val conditionWeather: TextView = root.findViewById(R.id.main_weather_data_condition)
        conditionWeather.text = it.condition

        val conditionData: ImageView = root.findViewById(R.id.main_weather_data_image)
        conditionData.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                conditionData.viewTreeObserver.removeOnGlobalLayoutListener(this)
                Picasso.with(requireContext())
                    .load(it.iconUrl)
                    .resize(conditionData.width, conditionData.height)
                    .centerInside()
                    .into(conditionData)
            }
        })

        val wind: TextView = root.findViewById(R.id.wind_data)
        (it.wind).also { wind.text = it }

        val pressure: TextView = root.findViewById(R.id.pressure_data)
        (it.pressure.toString() + getString(R.string.mm_hg)).also { pressure.text = it }

        val rainProbability: TextView = root.findViewById(R.id.rain_probability_data)
        (it.probability).also { rainProbability.text = it }
    }

    private fun runnableDeclineHomeLocation(): Runnable {
        return Runnable { }
    }

    private fun runnableAcceptHomeLocation(homeLocation: WeatherData?): Runnable {
        return Runnable { sharedViewModel.setHomeLocation(homeLocation?.toHomeCityData) }
    }

    private fun setSquare() {
        val layout =
            requireView().findViewById<ConstraintLayout>(R.id.main_weather_data_layout)

        layout.updateLayoutParams { width = layout.height }
    }
}