package com.johannstolz.weather_app.data.repository

import android.location.Location
import com.johannstolz.weather_app.data.MyResult
import com.johannstolz.weather_app.data.api.ApiHandler
import com.johannstolz.weather_app.data.model.Coord
import com.johannstolz.weather_app.data.model.WeatherData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeatherRepository(
    private val apiHandler: ApiHandler,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: WeatherRepository? = null

        fun getInstance(apiHandler: ApiHandler) =
            instance ?: synchronized(this) {
                instance ?: WeatherRepository(apiHandler).also { instance = it }
            }
    }


    suspend fun getWeatherData(city: String?): MyResult<Exception, WeatherData> =
        withContext(ioDispatcher) {
            return@withContext try {
                val weatherData = apiHandler.getWeatherData(city)
                MyResult.Value(weatherData)
            } catch (e: Exception) {
                MyResult.Error(e)
            }
        }

    suspend fun getWeatherData(location: Location): MyResult<Exception, WeatherData> =
        withContext(ioDispatcher) {
            return@withContext try {
                val weatherData = apiHandler.getWeatherData(
                    Coord(location.latitude, location.longitude)
                )
                MyResult.Value(weatherData)
            } catch (e: Exception) {
                MyResult.Error(e)
            }
        }
}