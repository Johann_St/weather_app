package com.johannstolz.weather_app.data.model

data class CityData(
    val city: City,
    val isHome: Boolean
)