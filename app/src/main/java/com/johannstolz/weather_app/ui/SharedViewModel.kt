package com.johannstolz.weather_app.ui

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.johannstolz.weather_app.Constants.Companion.NOT_SET
import com.johannstolz.weather_app.Constants.Companion.NOT_SET_NO_LOCATION
import com.johannstolz.weather_app.data.MyResult
import com.johannstolz.weather_app.data.model.CityData
import com.johannstolz.weather_app.data.source.CityDataLocalRepository
import com.johannstolz.weather_app.utils.NullDbDataException
import com.johannstolz.weather_app.utils.handleIOError
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SharedViewModel(private val localRepository: CityDataLocalRepository) : ViewModel() {
    companion object {
        private var instance: SharedViewModel? = null
        fun getInstance(repository: CityDataLocalRepository) =
            instance ?: synchronized(SharedViewModel::class.java) {
                instance ?: SharedViewModel(repository).also { instance = it }
            }
    }

    private val _homeLocation = MutableLiveData<String>()
    val homeLocation: LiveData<String>
        get() = _homeLocation
    private val _currentLocation = MutableLiveData<Location>()
    val currentLocation: LiveData<Location>
        get() = _currentLocation

    fun checkHomeLocationSet(isNoLocationData: Boolean) {
        viewModelScope.launch {
            delay(1000)
            when (val defaultCity = localRepository.getDefaultLocationCity()) {
                is MyResult.Value -> {
                    _homeLocation.value = defaultCity.value.city.name
                }
                is MyResult.Error -> {
                    if (defaultCity.error is NullDbDataException) {
                        _homeLocation.value =
                            if (!isNoLocationData) NOT_SET else NOT_SET_NO_LOCATION
                    }
                    handleIOError(defaultCity.error)
                }
            }
        }
    }

    fun setHomeLocation(currentLocation: CityData?) {
        viewModelScope.launch {
            when (val defaultCity =
                currentLocation?.let { localRepository.insertOrUpdateCityData(it) }) {
                    is MyResult.Value -> _homeLocation.value = currentLocation.city.name
                is MyResult.Error -> {
                    handleIOError(defaultCity.error)
                }
            }
        }
    }

    fun setLocationFromProvider(location: Location) {
        _currentLocation.value = location
    }
}