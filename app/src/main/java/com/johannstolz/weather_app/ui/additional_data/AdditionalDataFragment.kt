package com.johannstolz.weather_app.ui.additional_data

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.johannstolz.weather_app.R
import com.johannstolz.weather_app.ui.SharedViewModel
import com.johannstolz.weather_app.ui.SharedViewModelFactory
import com.johannstolz.weather_app.utils.InjectorUtils

class AdditionalDataFragment : Fragment() {
    private val sharedViewModel: SharedViewModel by activityViewModels() {
        SharedViewModelFactory(InjectorUtils.getCityDataRepository(requireContext()))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_additional, container, false)

        observeVieModel(root)

        return root
    }

    private fun observeVieModel(root: View) {
        val homeLocation: TextView = root.findViewById(R.id.home_location)
        val currentLocation: TextView = root.findViewById(R.id.current_location)
        sharedViewModel.homeLocation.observe(viewLifecycleOwner, { it ->
            (getString(R.string.home_location) + "\n" + it).also { homeLocation.text = it }
        })

        sharedViewModel.currentLocation.observe(viewLifecycleOwner, { it ->
            (getString(R.string.current_location) + "\n" + it).also { currentLocation.text = it }
        })
    }
}