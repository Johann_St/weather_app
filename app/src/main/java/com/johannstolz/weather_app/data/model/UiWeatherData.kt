package com.johannstolz.weather_app.data.model

data class UiWeatherData(
    val cityName: String,
    val iconUrl: String,
    val temperature: Double,
    val condition: String,
    val wind: String,
    val pressure: Int,
    val humidity: String,
    val probability: String
)