package com.johannstolz.weather_app.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.johannstolz.weather_app.BuildConfig
import com.johannstolz.weather_app.MainActivity
import com.johannstolz.weather_app.R
import timber.log.Timber

class MyLocationService {
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var location: Location

    companion object {
        @Volatile
        private var instance: MyLocationService? = null
        fun getInstance(): MyLocationService {
            return instance ?: synchronized(this) {
                instance ?: MyLocationService().also { instance = it }
            }
        }
    }

    fun onCreate(context: Context) {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    }

    fun onStart(context: Context) {
        if (!checkPermissions(context)) {
            requestPermissions(context as MainActivity)
        } else {
            getLastLocation(context as MainActivity)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation(mainActivity: MainActivity) {
        fusedLocationClient.lastLocation
            .addOnCompleteListener { taskLocation ->
                if (taskLocation.isSuccessful && taskLocation.result != null) {
                    location = taskLocation.result
                    mainActivity.setLastLocation(location)
                } else {
                    Timber.tag("MyLocation")
                        .e(taskLocation.exception, "getLastLocation:exception")
                    mainActivity.checkHomeLocation(true)
                    mainActivity.showSnackbar(R.string.no_location_detected)
                }
            }
    }


    private fun checkPermissions(context: Context) =
        context.let {
            ActivityCompat.checkSelfPermission(
                it,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        } == PackageManager.PERMISSION_GRANTED

    private fun startLocationPermissionRequest(mainActivity: MainActivity) {
        ActivityCompat.requestPermissions(
            mainActivity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    private fun requestPermissions(mainActivity: MainActivity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                mainActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        ) {
            mainActivity.showSnackbar(
                R.string.permission_rationale, android.R.string.ok
            ) {
                startLocationPermissionRequest(mainActivity)
            }

        } else {
            startLocationPermissionRequest(mainActivity)
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
        mainActivity: MainActivity
    ) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                (grantResults[0] == PackageManager.PERMISSION_GRANTED) -> getLastLocation(
                    mainActivity
                )
                else -> {
                    mainActivity.showSnackbar(
                        R.string.permission_denied_explanation, R.string.settings
                    ) {
                        val intent = Intent().apply {
                            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            data = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                            flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        }
                        mainActivity.startActivity(intent)
                    }
                }
            }
        }
    }

}