package com.johannstolz.weather_app.data.api

import com.johannstolz.weather_app.data.model.Coord
import com.johannstolz.weather_app.data.model.WeatherData

class ApiHandler(private val apiService: ApiService) {
    private val apiKey: String
        get() = "8456a20c5a357f30c51952b6bcfdfb32"

    suspend fun getWeatherData(city: String?): WeatherData =
        apiService.getWeatherData(city, apiKey)

    suspend fun getWeatherData(coord: Coord): WeatherData =
        apiService.getWeatherData(coord.lat, coord.lon, apiKey)
}