package com.johannstolz.weather_app.data.source

import androidx.room.*


@Dao
interface CityDataDao {
    @Query("SELECT * FROM city_data_table")
    suspend fun getCities(): List<RoomCityData>

    @Query("SELECT * FROM city_data_table WHERE isCurrentLocation = 1")
    suspend fun getDefaultLocationCity(): RoomCityData?

    @Delete
    suspend fun deleteCity(cityData: RoomCityData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdateCityData(cityData: RoomCityData): Long

    @Query("DELETE FROM city_data_table")
    suspend fun deleteAllCities()
}

