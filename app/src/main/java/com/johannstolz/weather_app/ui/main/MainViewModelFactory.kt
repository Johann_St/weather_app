package com.johannstolz.weather_app.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.johannstolz.weather_app.data.repository.WeatherRepository

class MainViewModelFactory(private val repository: WeatherRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("unchecked_cast")
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

    companion object {
        private var instance: MainViewModelFactory? = null
        fun getInstance(repository: WeatherRepository) =
            instance ?: synchronized(MainViewModelFactory::class.java) {
                instance ?: MainViewModelFactory(repository).also { instance = it }
            }
    }
}