package com.johannstolz.weather_app.data

sealed class MyResult<out E, out V> {

    data class Value<out V>(val value: V) : MyResult<Nothing, V>()
    data class Error<out E>(val error: E) : MyResult<E, Nothing>()

}