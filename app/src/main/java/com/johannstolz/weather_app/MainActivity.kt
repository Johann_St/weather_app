package com.johannstolz.weather_app

import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.johannstolz.weather_app.location.MyLocationService
import com.johannstolz.weather_app.ui.SharedViewModel
import com.johannstolz.weather_app.ui.SharedViewModelFactory
import com.johannstolz.weather_app.utils.InjectorUtils
import com.johannstolz.weather_app.utils.TimberImpl.Companion.initLogging


class MainActivity : AppCompatActivity() {

    private var myLocationService: MyLocationService? = null

    private val sharedViewModel: SharedViewModel by viewModels {
        SharedViewModelFactory.getInstance(InjectorUtils.getCityDataRepository(this))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myLocationService = MyLocationService.getInstance()
        myLocationService?.onCreate(this)
        setContentView(R.layout.activity_main)
        initLogging()
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        navView.setupWithNavController(navController)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        myLocationService?.onRequestPermissionsResult(
            requestCode, permissions, grantResults, this
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        myLocationService = null
    }

    fun checkHomeLocation(isNoLocationData: Boolean) {
        sharedViewModel.checkHomeLocationSet(isNoLocationData)
    }

    fun setLastLocation(location: Location?) {
        if (location != null) {
            checkHomeLocation(false)
            sharedViewModel.setLocationFromProvider(location)
        } else checkHomeLocation(true)
    }

    fun getLastLocation() {
        myLocationService?.onStart(this)
    }

    fun showSnackbar(
        snackStrId: Int,
        actionStrId: Int = 0,
        listener: View.OnClickListener? = null,
    ) {
        val snackbar = Snackbar.make(
            findViewById(android.R.id.content), getString(snackStrId),
            Snackbar.LENGTH_INDEFINITE
        )
        if (actionStrId != 0 && listener != null) {
            snackbar.setAction(getString(actionStrId), listener)
        }
        snackbar.show()
    }
}

