package com.johannstolz.weather_app.utils

import android.content.Context
import com.johannstolz.weather_app.data.api.ApiHandler
import com.johannstolz.weather_app.data.api.RetrofitBuilder
import com.johannstolz.weather_app.data.repository.WeatherRepository
import com.johannstolz.weather_app.data.source.AppDatabase
import com.johannstolz.weather_app.data.source.CityDataLocalRepository

object InjectorUtils {

    fun getCityDataRepository(context: Context): CityDataLocalRepository {
        return CityDataLocalRepository.getInstance(
            AppDatabase.getInstance(context.applicationContext).cityDao()
        )
    }

    fun getWeatherRepository(): WeatherRepository {
        return WeatherRepository.getInstance(ApiHandler(RetrofitBuilder.apiService))
    }
}