package com.johannstolz.weather_app.utils

import com.johannstolz.weather_app.BuildConfig
import timber.log.Timber

class TimberImpl {
    companion object {
        fun initLogging() {
            if (Timber.treeCount() != 0) return
            if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        }
    }
}