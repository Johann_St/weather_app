package com.johannstolz.weather_app.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.johannstolz.weather_app.data.source.CityDataLocalRepository

class SharedViewModelFactory(private val repository: CityDataLocalRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("unchecked_cast")
        if (modelClass.isAssignableFrom(SharedViewModel::class.java)) {
            return SharedViewModel.getInstance(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

    companion object {
        private var instance: SharedViewModelFactory? = null
        fun getInstance(repository: CityDataLocalRepository) =
            instance ?: synchronized(SharedViewModelFactory::class.java) {
                instance ?: SharedViewModelFactory(repository).also { instance = it }
            }
    }
}