package com.johannstolz.weather_app.data.api

import com.johannstolz.weather_app.data.model.WeatherData
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("forecast?")
    suspend fun getWeatherData(
        @Query("q") cityName: String?,
        @Query("appid") apiKey: String
    ): WeatherData

    @GET("forecast?")
    suspend fun getWeatherData(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") apiKey: String
    ): WeatherData

}