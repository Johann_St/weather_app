package com.johannstolz.weather_app.utils

import android.content.Context
import android.widget.Toast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.johannstolz.weather_app.R
import timber.log.Timber

fun makeShortToast(context: Context, text: String) {
    Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    Timber.d(text)

}

fun displayDialog(
    titleId: Int,
    messageId: Int,
    context: Context,
    runnableAccept: Runnable,
    runnableDecline: Runnable
) {
    MaterialAlertDialogBuilder(
        context,
        R.style.ThemeOverlay_MaterialComponents_Dialog
    ).setTitle(titleId)
        .setMessage(messageId)
        .setNegativeButton(R.string.decline) { _, _ ->
            runnableDecline.run()
        }
        .setPositiveButton(R.string.accept) { _, _ ->
            runnableAccept.run()
        }
        .show()
}

fun displayDialogOneBtn(
    titleId: Int,
    messageId: Int,
    context: Context,
    runnableAccept: Runnable,
) {
    MaterialAlertDialogBuilder(
        context,
        R.style.ThemeOverlay_MaterialComponents_Dialog
    ).setTitle(titleId)
        .setMessage(messageId)
        .setPositiveButton(R.string.accept) { _, _ ->
            runnableAccept.run()
        }
        .show()
}