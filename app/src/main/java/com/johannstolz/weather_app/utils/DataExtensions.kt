package com.johannstolz.weather_app.utils

import com.johannstolz.weather_app.R
import com.johannstolz.weather_app.data.model.*
import com.johannstolz.weather_app.data.source.RoomCityData


internal fun List<RoomCityData>.toCityDataListFromRoomCityList(): List<CityData> =
    this.flatMap { listOf(it.toCityData) }


internal val RoomCityData.toCityData: CityData
    get() = CityData(
        City(Coord(lat, lon), country, id, name, population, sunrise, sunset, timezone),
        isHomeLocation
    )
internal val CityData.toRoomCityData: RoomCityData
    get() = RoomCityData(
        city.id,
        city.coord.lat,
        city.coord.lon,
        city.country,
        city.name,
        city.population,
        city.sunrise,
        city.sunset,
        city.timezone,
        isHome
    )

internal val WeatherData.toHomeCityData: CityData
    get() = CityData(
        city,
        true
    )
internal val WeatherData.toUiWeatherData: UiWeatherData
    get() = UiWeatherData(
        city.name,
        "http://openweathermap.org/img/w/$getIconUrl.png",
        list[0].main.temp,
        list[0].weather[0].description,
        list[0].wind.speed.toString() + " m/s " + toDirectionFromDegree(list[0].wind.deg),
        list[0].main.pressure,
        list[0].main.humidity.toString() + " %",
        list[0].pop.toString() + " %"
    )

val WeatherData.getIconUrl: String
    get() = list[0].weather[0].icon

fun toDirectionFromDegree(degree: Int): String {
    val prefix = " "
    return when {
        degree > 337.5 -> prefix + "Northerly"
        degree > 292.5 -> prefix + "North Westerly"
        degree > 247.5 -> prefix + "Westerly"
        degree > 202.5 -> prefix + "South Westerly"
        degree > 157.5 -> prefix + "Southerly"
        degree > 122.5 -> prefix + "South Easterly"
        degree > 67.5 -> prefix + "Easterly"
        degree > 22.5 -> prefix + "North Easterly"
        else -> prefix + "Northerly"
    }
}