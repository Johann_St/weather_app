package com.johannstolz.weather_app.data.source

import com.johannstolz.weather_app.data.MyResult
import com.johannstolz.weather_app.data.model.CityData

interface ICityDataRepository {
    suspend fun getCities(): MyResult<Exception, List<CityData>>
    suspend fun getDefaultLocationCity(): MyResult<Exception, CityData>
    suspend fun deleteCity(cityData: CityData): MyResult<Exception, Unit>
    suspend fun insertOrUpdateCityData(cityData: CityData): MyResult<Exception, Long>
    suspend fun deleteAllCities(): Any
}