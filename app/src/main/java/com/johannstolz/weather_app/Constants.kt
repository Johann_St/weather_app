package com.johannstolz.weather_app

class Constants {
    companion object {
        const val BAD_RESPONSE = "City not found, try again"
        const val DATABASE_NAME = "weather-db"
        const val NOT_SET = "value_not_set"
        const val NOT_SET_NO_LOCATION = "value_and_location_not_set"
    }
}