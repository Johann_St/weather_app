package com.johannstolz.weather_app.ui.main

import android.annotation.SuppressLint
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.johannstolz.weather_app.data.MyResult
import com.johannstolz.weather_app.data.model.WeatherData
import com.johannstolz.weather_app.data.repository.WeatherRepository
import com.johannstolz.weather_app.ui.GlobalViewModel
import com.johannstolz.weather_app.utils.handleIOError
import kotlinx.coroutines.launch

class MainViewModel(private val weatherRepository: WeatherRepository) : GlobalViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String>
        get() = _errorMessage

    private val _weatherData = MutableLiveData<WeatherData>()
    val weatherData: LiveData<WeatherData>
        get() = _weatherData

    internal var isHoldingSearchData = false

    @SuppressLint("NullSafeMutableLiveData")
    fun fetchData(city: String?) {
        viewModelScope.launch {
            when (val weatherData = weatherRepository.getWeatherData(city)) {
                is MyResult.Value -> {
                    _weatherData.value = weatherData.value
                    isHoldingSearchData = true
                }
                is MyResult.Error -> {
                    handleIOError(weatherData.error)
                    _errorMessage.value = weatherData.error.toString()
                }
            }
        }
    }

    @SuppressLint("NullSafeMutableLiveData")
    fun fetchData(location: Location) {
        viewModelScope.launch {
            when (val weatherData = weatherRepository.getWeatherData(location)) {
                is MyResult.Value -> {
                    _weatherData.value = weatherData.value
                    isHoldingSearchData = false
                }
                is MyResult.Error -> {
                    handleIOError(weatherData.error)
                    _errorMessage.value = weatherData.error.toString()
                }
            }
        }
    }

}