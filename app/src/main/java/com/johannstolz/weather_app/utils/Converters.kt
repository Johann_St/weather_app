package com.johannstolz.weather_app.utils

import kotlin.math.roundToInt

val Double.celsiusToF: Int
    get() = (this * 1.8 + 32).roundToInt()

val Double.fahrenheitToC: Int
    get() = ((this - 32) / 1.8).roundToInt()

val Double.kelvinToF: Int
    get() = ((this - 273.15) * 1.8 + 32).roundToInt()

val Double.kelvinToC: Int
    get() = (this - 273).roundToInt()
