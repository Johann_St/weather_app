package com.johannstolz.weather_app.data.source

import com.johannstolz.weather_app.data.MyResult
import com.johannstolz.weather_app.data.model.CityData
import com.johannstolz.weather_app.utils.NullDbDataException
import com.johannstolz.weather_app.utils.toCityData
import com.johannstolz.weather_app.utils.toCityDataListFromRoomCityList
import com.johannstolz.weather_app.utils.toRoomCityData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class CityDataLocalRepository internal constructor(
    private val cityDataDao: CityDataDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ICityDataRepository {
    companion object {
        @Volatile
        private var instance: CityDataLocalRepository? = null
        fun getInstance(cityDataDao: CityDataDao) =
            instance
                ?: synchronized(this) {
                    instance
                        ?: CityDataLocalRepository(
                            cityDataDao
                        ).also { instance = it }
                }
    }

    override suspend fun getCities(): MyResult<Exception, List<CityData>> =
        withContext(ioDispatcher) {
            return@withContext try {
                val cities = cityDataDao.getCities().toCityDataListFromRoomCityList()
                MyResult.Value(cities)
            } catch (e: Exception) {
                MyResult.Error(e)
            }
        }

    override suspend fun getDefaultLocationCity(): MyResult<Exception, CityData> =
        withContext(ioDispatcher) {
            return@withContext try {
                val city = cityDataDao.getDefaultLocationCity()?.toCityData
                if (city == null) {
                    MyResult.Error(NullDbDataException("home city not set yet"))
                } else {
                    MyResult.Value(city)
                }
            } catch (e: Exception) {
                MyResult.Error(e)
            }
        }

    override suspend fun deleteCity(cityData: CityData): MyResult<Exception, Unit> =
        withContext(ioDispatcher) {
            return@withContext try {
                val roomCityData = cityData.toRoomCityData
                val nounDeleted = cityDataDao.deleteCity(roomCityData)
                MyResult.Value(nounDeleted)
            } catch (e: Exception) {
                MyResult.Error(e)
            }
        }

    override suspend fun insertOrUpdateCityData(cityData: CityData): MyResult<Exception, Long> =
        withContext(ioDispatcher) {
            return@withContext try {
                val toRoomCityData = cityData.toRoomCityData
                val insertOrUpdateCityData = cityDataDao.insertOrUpdateCityData(toRoomCityData)
                MyResult.Value(insertOrUpdateCityData)
            } catch (e: Exception) {
                MyResult.Error(e)
            }
        }

    override suspend fun deleteAllCities(): Any = withContext(ioDispatcher) {
        return@withContext try {
            cityDataDao.deleteAllCities()
        } catch (e: Exception) {
            MyResult.Error(e)
        }
    }

}
