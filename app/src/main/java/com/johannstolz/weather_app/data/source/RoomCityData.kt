package com.johannstolz.weather_app.data.source

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "city_data_table"
)

data class RoomCityData constructor(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "lat") val lat: Double,
    @ColumnInfo(name = "lon") val lon: Double,
    @ColumnInfo(name = "country") val country: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "population") val population: Int,
    @ColumnInfo(name = "sunrise") val sunrise: Int,
    @ColumnInfo(name = "sunset") val sunset: Int,
    @ColumnInfo(name = "timezone") val timezone: Int,
    @ColumnInfo(name = "isCurrentLocation") val isHomeLocation: Boolean
)


