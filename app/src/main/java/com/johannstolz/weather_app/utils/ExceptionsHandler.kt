package com.johannstolz.weather_app.utils

import timber.log.Timber

class NullDbDataException(message: String?) : Exception(message) {

}

fun handleIOError(error: Exception) {
    Timber.e(error)
}

